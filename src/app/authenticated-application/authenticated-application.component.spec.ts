import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticatedApplicationComponent } from './authenticated-application.component';

describe('AuthenticatedApplicationComponent', () => {
  let component: AuthenticatedApplicationComponent;
  let fixture: ComponentFixture<AuthenticatedApplicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthenticatedApplicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticatedApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
