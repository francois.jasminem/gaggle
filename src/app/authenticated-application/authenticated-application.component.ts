import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-authenticated-application',
  templateUrl: './authenticated-application.component.html',
  styleUrls: ['./authenticated-application.component.scss']
})
export class AuthenticatedApplicationComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    const allCookies = document.cookie;
    if (!allCookies.includes('gaggle-authenticated')) {
      this.router.navigate(['/sign-on'])
    }

  }

}
