import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SignonComponent} from "./signon/signon.component";
import {AuthenticatedApplicationComponent} from "./authenticated-application/authenticated-application.component";

const routes: Routes = [
  {path: '', redirectTo: 'sign-on', pathMatch: 'full'},
  {path: "sign-on", component: SignonComponent},
  {path: "app", component: AuthenticatedApplicationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
