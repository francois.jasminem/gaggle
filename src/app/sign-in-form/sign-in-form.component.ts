import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.scss']
})
export class SignInFormComponent implements OnInit {

  constructor(private fb: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
  }

  signInForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    rememberMe: [false],
  }, {updateOn: "blur"});

  showPassword: boolean = false;

  togglePassword(): void {
    this.showPassword = !this.showPassword;
  }

  onSubmit(): void {
    if (this.signInForm.valid) {
      document.cookie = "gaggle-authenticated";
      this.router.navigate(['/app']);
    } else {
      Object.keys(this.signInForm.controls).forEach(field => {
        const control = this.signInForm.get(field);
        control?.markAsTouched({onlySelf: true})
      })
    }
  }

  clearValue(fieldName: string): void {
    this.signInForm.patchValue({
      [fieldName]: ""
    });
  }
}
