import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SignInFormComponent } from './sign-in-form.component';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";

describe('SignInFormComponent', () => {
  let component: SignInFormComponent;
  let fixture: ComponentFixture<SignInFormComponent>;
  let formBuilder: object;

  beforeEach(async () => {
    formBuilder = {
      group: () => {}
    };
    // was unable to get this router spy to work
    // followed the setup described here but was unsuccessful https://angular.io/guide/testing-components-scenarios#routing-component
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    await TestBed.configureTestingModule({
      declarations: [ SignInFormComponent ],
      providers: [
        {provide: FormBuilder, useValue: formBuilder},
        {provider: Router, useValue: routerSpy}
        ]
    })
    .compileComponents();

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
