import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.scss']
})
export class NewAccountComponent implements OnInit {
  @Output() closeModal = new EventEmitter();
  @Input() showModal: boolean = false;
  showPassword: boolean = false;

  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;
  }

  newAccountForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]]
  }, {updateOn: "blur"});


  onSubmit(): void {
    if (this.newAccountForm.valid) {
      document.cookie = "gaggle-authenticated";
      this.router.navigate(['/app']);
    } else {
      Object.keys(this.newAccountForm.controls).forEach(field => {
        const control = this.newAccountForm.get(field);
        control?.markAsTouched({onlySelf: true})
      })
    }
  }

  clearValue(fieldName: string): void {
    this.newAccountForm.patchValue({
      [fieldName]: ""
    });
  }
}
