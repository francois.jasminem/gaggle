import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule} from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignonComponent } from './signon/signon.component';
import { NewAccountComponent } from './new-account/new-account.component';
import { SignInFormComponent } from './sign-in-form/sign-in-form.component';
import { AuthenticatedApplicationComponent } from './authenticated-application/authenticated-application.component';

@NgModule({
  declarations: [
    AppComponent,
    SignonComponent,
    NewAccountComponent,
    SignInFormComponent,
    AuthenticatedApplicationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
