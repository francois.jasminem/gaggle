import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signon',
  templateUrl: './signon.component.html',
  styleUrls: ['./signon.component.scss']
})
export class SignonComponent implements OnInit {

  showNewAccount: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  openNewAccountModal(): void {
    this.showNewAccount = true;
  }

  closeNewAccountModal(): void {
    this.showNewAccount = false;
  }
}
