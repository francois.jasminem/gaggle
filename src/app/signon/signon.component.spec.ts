import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignonComponent } from './signon.component';

describe('SignonComponent', () => {
  let component: SignonComponent;
  let fixture: ComponentFixture<SignonComponent>;
  let compiled: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignonComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the gaggle logo', () => {
    expect(compiled.querySelector('img [title="Gaggle Logo"]')).toBeDefined();
  });

  it('should render the background', () => {
    expect(compiled.querySelector('.background')).toBe
  });

  it('should render the Register button', () => {
    expect(compiled.querySelector('button').textContent).toContain('Register');
  });

  it('should render the Forgot password? button', () => {
    console.log(compiled.querySelectorAll('button'));
    expect(compiled.querySelectorAll('button')[1].textContent).toContain('Forgot password?');
  });

  it('should open the new-account modal when pressing the Register button', () => {
    // I was not sure how to test this. In React, the tests renders the nested components so I could follow through with the assertions outlined below.
    // When I log out the rendered output (the compiled variable), the <app-new-account> tag or preceeding div's *ngIf output does not look any different before or after the click.
    // I tried debugging but got a whole bunch of stuff that I was not sure how to google.

    const registerButton = compiled.querySelector('button');

    expect(compiled.querySelector('h1')).not.toBeDefined();
    registerButton.click();
    expect(compiled.querySelector('h1').textContent).toContain('New Account');
  });
});
